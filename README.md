# Proyecto Ingenieria de Software

### Retrospectiva 2024-1 INF225: [Retrospectiva](https://gitlab.com/nicolas.gomezma/proyecto-analisis-y-diseno/-/wikis/Retrospectiva)

### Cambios Relevantes post-reunión: [Cambios 2024-1](https://gitlab.com/nicolas.gomezma/proyecto-analisis-y-diseno/-/wikis/Cambios-generados-por-el-cliente)

### Enlace a [Video hito 4 INF225 2024-1](https://youtu.be/_B9sLCVyYwI)

## Grupo 4

Profesor:
- Wladimir Ormazábal

Tutora:
- Camila Norambuena

Este es el repositorio del grupo 4, cuyos integrantes son:
- Claudio Villagrán (202021016-0)
- Alejandro Cubillos (202073123-3)
- Clemente Mujica (202104008-0)
- Cristóbal Moraga (202130008-2)

## Proyecto a continuar

Se continuará el proyecto alojado en este repositorio, el cual corresponde a la gestión de insumos de la tienda de "Veysa Manualidades", en específico, el proyecto del paralelo 1 realizado el semestre anterior (2023-2) del ramo "Análisis y Diseño de Software".

## Wiki

Puede acceder a la wiki mediante el siguiente [enlace](https://gitlab.com/nicolas.gomezma/proyecto-analisis-y-diseno/-/wikis/home)



## Videos

- [Video de reunión con cliente](https://www.youtube.com/watch?v=LHmE2B2Bumw)

- [Video presentación avance 1](https://www.youtube.com/watch?v=qzUVMzAlvUM)

- [Video presentación avance 2](https://youtu.be/ewbHhYOZPAQ)

- [Video presentación final Hito 7](https://youtu.be/UNhrCVElHJE)

-------------------

- [Video hito 4 INF225](https://youtu.be/_B9sLCVyYwI)


## Aspectos técnicos relevantes

- Tecnologia frontend a utilizar: React
- Se realizan los avances del proyecto en la branch "base".
- Se utiliza la API fetch para obtener datos de las API's creadas por nosotros en javascript.

## Pasos para levantar el proyecto

- Como se explico anteriormente, lo avances estan en la branch "base" de este repositorio, por lo tanto hay que descargar los archivos que hay dentro de esta branch (o clonar el repositorio).

- Luego se continua con los pasos para montar la base de datos (descritos por los ayudantes), se inicia levantando la imagen de mysql en docker. En una terminal escriba el siguiente comando:
```
docker run -p 3306:3306 --name mysql -e MYSQL_ROOT_PASSWORD=password -d mysql
```

- Una vez creada la imagen, crearemos las bases de datos. Para esto debemos entrar en el contendor, con el siguiente comando:
```
docker exec -it mysql mysql -uroot -p
```
Deben ingresar la clave que en este caso es **password**

- Una vez dentro del contenedor podemos crear las bases de datos:
```
create database BD01_PRODUCTOS;
```
```
create database BD01_INVENTARIO;
```

- Ahora dentro de la terminal hay que entrar en la carpeta API_PRODUCTOS y ejecutar el siguiente comando:
```
docker-compose up --build -d
```
- Dentro de la carpeta API_INVENTARIO hay que usar el mismo comando anterior.

- Luego, en algun navegador hay que colocar las siguientes dos rutas:
```
localhost:8080/createProducto
```
```
localhost:8081/createInsumo
```

- Finalmente dentro de la carpeta proyecto1-react, ejecutar los siguientes comandos:
```
npm install
```
```
npm start
```


