import unittest
import requests

class TestAPI(unittest.TestCase):
    base_url_insumo = "http://localhost:8081"
    base_url_producto = "http://localhost:8080"

    @classmethod
    def setUpClass(cls):
        """Preparar condiciones iniciales para todas las pruebas en esta clase."""
        cls.agregar_insumo_inicial()
        cls.agregar_producto_inicial()

    @classmethod
    def agregar_insumo_inicial(cls):
        url = f"{cls.base_url_insumo}/agregarInsumo"
        insumos_iniciales = [
            {"nombre": "Lana", "stock": 10, "precio_unitario": 20000},
            {"nombre": "Pintura", "stock": 3, "precio_unitario": 590},
            {"nombre": "Cinta adhesiva", "stock": 3, "precio_unitario": 600}
        ]
        for insumo in insumos_iniciales:
            requests.post(url, json=insumo)

    @classmethod
    def agregar_producto_inicial(cls):
        url = f"{cls.base_url_producto}/agregarProducto"
        productos_iniciales = [
            {"nombre": "Pastel", "precio": 30000, "stock": 50, "categoria": "Comida", "por_encargo": False},
            {"nombre": "Peluche", "precio": 45000, "stock": 10, "categoria": "Juguete", "por_encargo": False},
            {"nombre": "Muñeco", "precio": 20000, "stock": 10, "categoria": "Juguete", "por_encargo": False}
        ]
        for producto in productos_iniciales:
            requests.post(url, json=producto)

    @classmethod
    def tearDownClass(cls):
        """Limpiar el entorno después de que todas las pruebas en la clase se hayan ejecutado."""
        cls.eliminar_todos_los_productos()
        cls.eliminar_todos_los_insumos()

    @classmethod
    def eliminar_todos_los_insumos(cls):
        respuesta = requests.get(f"{cls.base_url_insumo}/verInsumos")
        if respuesta.ok:
            insumos = respuesta.json()
            for insumo in insumos:
                requests.delete(f"{cls.base_url_insumo}/eliminarInsumo/{insumo.get('id')}")

    @classmethod
    def eliminar_todos_los_productos(cls):
        respuesta = requests.get(f"{cls.base_url_producto}/verProductos")
        if respuesta.ok:
            productos = respuesta.json()
            for producto in productos:
                requests.delete(f"{cls.base_url_producto}/eliminarProducto/{producto.get('id')}")


    def test_actualizar_insumo(self):
        url = f"{self.base_url_insumo}/verInsumos"
        respuesta = requests.get(url)
        insumos = respuesta.json() if respuesta.ok else []
        if insumos:
            insumo_id = insumos[0].get('id')
            if insumo_id:
                url = f"{self.base_url_insumo}/actualizarInsumo/{insumo_id}"
                data = {"nombre": "Lana actualizada", "stock": 15, "precio_unitario": 20500}
                respuesta = requests.put(url, json=data)
                self.assertEqual(respuesta.status_code, 200)
            else:
                self.fail("No se encontró 'id' para el insumo.")
        else:
            self.fail("No se devolvieron insumos desde la API.")

    def test_eliminar_insumo(self):
        busqueda = "Pintura"
        url = f"{self.base_url_insumo}/verInsumosPorNombre?nombre={busqueda}"
        insumo = requests.get(url)
        
        insumo_id = insumo.json()[0]['id']
        url = f"{self.base_url_insumo}/eliminarInsumo/{insumo_id}"
        response = requests.delete(url)
        self.assertEqual(response.status_code, 200)

    def test_ver_insumos_por_nombre(self):
        busqueda = "Cinta adhesiva"
        url = f"{self.base_url_insumo}/verInsumosPorNombre?nombre={busqueda}"
        response = requests.get(url)
        self.assertEqual(response.status_code, 200)

    # API DE PRODUCTOS
    def test_agregar_producto(self):
        url = f"{self.base_url_producto}/agregarProducto"
        data = {
            "nombre": "Pastel",
            "precio": 30000,
            "stock": 50,
            "categoria": "Comida",
            "por_encargo": False
        }
        response = requests.post(url, json=data)
        self.assertEqual(response.status_code, 200)
        
        data = {
            "nombre": "Peluche",
            "precio": 45000,
            "stock": 10,
            "categoria": "Juguete",
            "por_encargo": False
        }
        response = requests.post(url, json=data)
        self.assertEqual(response.status_code, 200)
        
        data = {
            "nombre": "Muñeco",
            "precio": 20000,
            "stock": 10,
            "categoria": "Juguete",
            "por_encargo": False
        }
        response = requests.post(url, json=data)
        self.assertEqual(response.status_code, 200)

    def test_actualizar_insumo(self):
        url = f"{self.base_url_insumo}/verInsumos"
        respuesta = requests.get(url)
        insumos = respuesta.json() if respuesta.ok else []
        if insumos:
            insumo_id = insumos[0].get('id')
            if insumo_id:
                url = f"{self.base_url_insumo}/actualizarInsumo/{insumo_id}"
                data = {"nombre": "Lana actualizada", "stock": 15, "precio_unitario": 20500}
                respuesta = requests.put(url, json=data)
                self.assertEqual(respuesta.status_code, 200)
            else:
                self.fail("No se encontró 'id' para el insumo.")
        else:
            self.fail("No se devolvieron insumos desde la API.")

    def test_eliminar_insumo(self):
        busqueda = "Pintura"
        url = f"{self.base_url_insumo}/verInsumosPorNombre?nombre={busqueda}"
        response = requests.get(url)
        insumos = response.json() if response.ok else []

        if insumos:
            insumo_id = insumos[0].get('id')
            if insumo_id:
                url = f"{self.base_url_insumo}/eliminarInsumo/{insumo_id}"
                response = requests.delete(url)
                self.assertEqual(response.status_code, 200)
            else:
                self.fail("No se encontró 'id' para el insumo.")
        else:
            self.fail("No se devolvieron insumos desde la API.")

    def test_ver_productos_por_nombre(self):
        busqueda = "Muñeco"
        url = f"{self.base_url_producto}/verProductosPorNombre?nombre={busqueda}"
        response = requests.get(url)
        self.assertEqual(response.status_code, 200)

    def test_ver_insumos(self):
        url = f"{self.base_url_insumo}/verInsumos"
        response = requests.get(url)
        self.assertEqual(response.status_code, 200)

    def test_ver_productos(self):
        url = f"{self.base_url_producto}/verProductos"
        response = requests.get(url)
        self.assertEqual(response.status_code, 200)

    def test_agregar_insumo_negativo(self):
        """Test para intentar agregar un insumo con stock negativo y verificar el manejo de error."""
        url = f"{self.base_url_insumo}/agregarInsumo"
        data = {
            "nombre": "Papel",
            "stock": -1,
            "precio_unitario": 100
        }
        response = requests.post(url, json=data)
        self.assertNotEqual(response.status_code, 200)

    def test_agregar_insumo_correcto(self):
        """Test para intentar agregar un insumo con stock negativo y verificar el manejo de error."""
        url = f"{self.base_url_insumo}/agregarInsumo"
        data = {
            "nombre": "Carton",
            "stock": 3,
            "precio_unitario": 500
        }
        response = requests.post(url, json=data)
        self.assertEqual(response.status_code, 200)
    
    def test_eliminar_insumo_inexistente(self):
        insumo_id_inexistente = 99999  
        url = f"{self.base_url_insumo}/eliminarInsumo/{insumo_id_inexistente}"
        response = requests.delete(url)
        self.assertEqual(response.status_code, 404)
        expected_message = "Insumo no encontrado"
        actual_message = response.json().get('message', '')
        self.assertIn(expected_message, actual_message)

if __name__ == '__main__':
    unittest.main()
